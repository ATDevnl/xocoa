<?php 
get_header(); 
the_post(); 
?>
<div class="container blog__container text-left">
  <h1 class="text-center"><?php the_title(); ?></h1>
  <?php the_post_thumbnail('large', array('class' => 'img-fluid')); ?>
  <?php
    the_content( ); 
  ?>
</div>
<?php get_footer(); ?>