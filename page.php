<?php 
get_header(); 
the_post(); 
?>
<div class="container blog__container text-left">
  <h1 class="text-center"><?php the_title(); ?></h1>
  <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-fluid">
  <?php the_post_thumbnail_url(); ?>
  <?php
    the_content(); 
  ?>
</div>
<?php get_footer(); ?>