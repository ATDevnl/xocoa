<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>
<?php bloginfo('name'); echo(' | '); if(is_front_page()) { 
  bloginfo('description'); 
  } else{
    the_title();
    } ?> 

</title>
<?php wp_head(); ?>
</head>

<body <?php body_class( 'class-name' ); ?>>
<?php wp_body_open(); ?>

<nav class="navbar navbar--container fixed-top">
  <div class="navbar--logo">
    <?php $custom_logo_id = get_theme_mod( 'custom_logo' );$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );?>
    <img src="<?php echo $image[0]; ?>" alt="" class="img-fluid">
  </div>

  <!-- <div class="navbar__links">
    <ul>
      <li class="navbar__link navbar__link--active"><a href="">Home</a></li>
      <li class="navbar__link"><a href="">About Xocoa</a></li>
      <li class="navbar__link"><a href="">Faq</a></li>
      <li class="navbar__link"><a href="">Honest stories</a></li>
      <li class="navbar__link"><a href="">Contact</a></li>
    </ul>
  </div> -->
  <?php wp_nav_menu( array( 
    'theme_location' => 'header-menu',
    'add_li_class'  => 'navbar__link',
    'menu_class' => 'navbar__links d-md-block d-none',
    ) ); ?>
</nav>
<div class="navbar__mobiel d-flex align-items-center d-md-none d-block" id="toggleMe">
  <?php wp_nav_menu( array( 
    'theme_location' => 'header-menu',
    'add_li_class'  => 'navbar__mobiel__link',
    'menu_class' => 'navbar__mobiel__links',
    ) ); ?>
</div>

<div class="menutoggler d-md-none d-block">
  <span class="first"></span>
  <span class="second"></span>
  <span class="third"></span>
  <div class="menutoggler__icon">
    
  </div>
</div>