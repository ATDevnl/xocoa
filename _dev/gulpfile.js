const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefix = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    sasslint = require('gulp-sass-lint'),
    wait = require('gulp-wait');

const files = {
    sass: {
        source: [
            'scss/**/**.scss',
        ],
        dest: '../src/style',
    }
};

// Task for overall styles
gulp.task('default', function () {
    gulp.src(files.sass.source)
        .pipe(wait(1000))
        .pipe(sourcemaps.init())
        .pipe(sasslint())
        .pipe(sasslint.format())
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'compressed'
        }))
        .pipe(autoprefix({
        }))
        .pipe(concat('themeStyle.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(files.sass.dest));
});

function watch() {
    // Compile overall styles
    gulp.watch(files.sass.source, ['default']);
}

gulp.task('watch', ['default'], function () {
    return watch(true);
});

gulp.task('ci', ['default']);
