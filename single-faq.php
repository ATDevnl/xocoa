<?php 
get_header(); 
the_post(); 
?>
<div class="container blog__container">
  <h1 class="text-center"><?php the_title(); ?></h1>
  <?php
    the_content(); 
  ?>
  <?php do_action('back_button'); ?>
</div>
<?php get_footer(); ?>