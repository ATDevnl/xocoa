AOS.init();
var $ = jQuery;
$(function(){
  var lastScrollTop = 0, delta = 5;
  $(window).scroll(function(event){
     var st = $(this).scrollTop();
     
     if(Math.abs(lastScrollTop - st) <= delta)
        return;
     
     if (st > lastScrollTop){
         // downscroll code
        //  console.log('scroll down');
         $('.navbar__links').addClass('navbar__links--up');
     } else {
        // upscroll code
        // console.log('scroll up');
        $('.navbar__links').removeClass('navbar__links--up');
     }
     lastScrollTop = st;
  });
});

$( document ).ready(function() {
  $('.switcher').click(function() {
    if ( $( this ).hasClass( "toggler" ) ) {
      $('.btn.switcher').toggleClass('btn-primary');
      $('.btn.switcher').toggleClass('btn-secundary');
      $('.btn.switcher').toggleClass('toggler');

      $('.icon-btn').toggleClass('d-none');
      $('.flavours__chocolate').toggleClass('d-none');

  }
  });

  $( ".woocommerce-product-details__short-description").prepend( $( "#prependTitle"  ) );

  $('.menutoggler').click(function() {
    $(this).toggleClass('active');
    $('#toggleMe').toggleClass('active');
  });
  
});