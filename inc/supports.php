<?php
// Adding support to this theme.
add_theme_support( 'custom-logo' ); 
add_theme_support( 'post-thumbnails' );
add_post_type_support( 'page', 'excerpt' );