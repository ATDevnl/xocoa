<?php
function xocoa_register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' ),
      'footer-sub-menu' => __( 'Footer Sub Menu' )
     )
   );
 }
 add_action( 'init', 'xocoa_register_my_menus' );

 function xocoa_add_additional_class_on_li($classes, $item, $args) {
  if(isset($args->add_li_class)) {
      $classes[] = $args->add_li_class;
  }
  return $classes;
}
add_filter('nav_menu_css_class', 'xocoa_add_additional_class_on_li', 1, 3);