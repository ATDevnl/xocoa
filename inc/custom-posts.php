<?php
// Our custom post type function
function xocoa_create_posttype() {
 
  register_post_type( 'faq',
  // CPT Options
      array(
          'labels' => array(
              'name' => __( 'FAQ' ),
              'singular_name' => __( 'Question' )
          ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array('slug' => 'faq'),
          'show_in_rest' => true,
          'taxonomies' => array('topics', 'category' ),
      )
  );
}
// Hooking up our function to theme setup
add_action( 'init', 'xocoa_create_posttype' );