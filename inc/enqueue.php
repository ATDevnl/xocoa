<?php
$dir = get_template_directory_uri();

function xocoa_theme_scripts_styles() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style( 'bootstrap', get_template_directory_uri( ) . '/src/style/bootstrap.css', array(), '0.1.0', 'all');
  wp_enqueue_style( 'aosCSS', 'https://unpkg.com/aos@2.3.1/dist/aos.css', array(), '2.3.1', 'all');
  wp_enqueue_style( 'materialdesignicons', 'https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.1.45/css/materialdesignicons.min.css', array(), '0.1.0', 'all');
  wp_enqueue_style( 'la-theme-style', get_template_directory_uri( ) . '/src/style/themeStyle.css', array(), '0.1.0', 'all');

  wp_enqueue_script('jquery');
  wp_enqueue_script( 'propper', get_template_directory_uri() . '/src/javascript/popper.js', array(), '1.0.0', true );
  wp_enqueue_script( 'aosJS', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array(), '2.3.1', true );
  wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/src/javascript/bootstrap.js', array(), '1.0.0', true );
  wp_enqueue_script( 'la-theme-script', get_template_directory_uri() . '/src/javascript/script.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'xocoa_theme_scripts_styles' );