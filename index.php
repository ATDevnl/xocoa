<?php get_header(); ?>
<div class="container search__results">
  <h1>Search results</h1>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <h2><?php the_title(); ?></h2>
  <?php      
    the_content(); // displays whatever you wrote in the wordpress editor
    endwhile; endif; //ends the loop
  ?>
</div>

<?php get_footer(); ?>  