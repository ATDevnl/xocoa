
<footer id="colophon" role="contentinfo" class="footer">
  <ul class="footer__socials">
    <li class="footer__socials--link"><a href=""><img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Facebook.svg" alt=""></a></li>
    <li class="footer__socials--link"><a href=""><img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Instagram.svg" alt=""></a></li>
    <li class="footer__socials--link"><a href=""><img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Youtube.svg" alt=""></a></li>
  </ul>
<?php wp_nav_menu( array( 
    'theme_location' => 'footer-menu',
    'add_li_class'  => 'footer__menu--link',
    'menu_class' => 'footer__menu',
    ) ); ?>
    <?php wp_nav_menu( array( 
    'theme_location' => 'footer-sub-menu',
    'add_li_class'  => 'footer__submenu--link',
    'menu_class' => 'footer__submenu',
    ) ); ?>
</footer><!-- #colophon -->

<?php wp_footer(); ?>
</body>
</html>