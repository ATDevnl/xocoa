<div class="container featured">
  <h2>Newest story</h2>
    <?php
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 1, // Number of recent posts thumbnails to display
        'post_status' => 'publish' // Show only the published posts
    ));
    foreach($recent_posts as $post) : ?>
            <a href="<?php echo get_permalink($post['ID']) ?>">
            <div class="row align-items-end featured__container">
              <div class="col-md-5 featured__container--left">
                <h3><?php echo $post['post_title'] ?></h3>
                <?php echo get_the_post_thumbnail($post['ID'] ,'full', ['class' => 'img-fluid']); ?>
              </div>
              <div class="col-md-7 featured__container--right">
                <p><?php echo $post['post_date'] ?></p>
                <p><?php echo $post['post_excerpt'] ?></p>
                <buton class="icon-btn first">Read more <img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Arrow.svg" alt=""> </button>
            </div>
            </div>
            
              
              
              <!-- <?php print_r($post) ?> -->
            </a>
    <?php endforeach; wp_reset_query(); ?>
</div>