<div class="container">
<div class="row storieshome__container">
  
    <?php
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => -1, // Number of recent posts thumbnails to display
        'post_status' => 'publish' // Show only the published posts
    ));
    foreach($recent_posts as $post) : ?>
        <div class="col-md-6 storieshome__story">
            <a href="<?php echo get_permalink($post['ID']) ?>">
                <?php echo get_the_post_thumbnail($post['ID'], 'full',['class' => 'img-fluid']); ?>
                <span class="storieshome__caption">
                  <h3 class="storieshome__caption--text d-inline"><?php echo $post['post_title'] ?></h3>
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Arrow.svg" alt="" class="float-right d-inline">
                </span>
            </a>
    </div>
    <?php endforeach; wp_reset_query(); ?>
    </div>
</div>