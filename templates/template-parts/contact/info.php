<div class="container">
  <h2>Contact us</h2>
  <ul class="contact__list">
    <li><img src="<?php echo get_stylesheet_directory_uri() ?>/icons/locate.svg" alt=""></i><p class="d-inline"> Van Galenstraat 19 7511JL  Enschede</p></li>
    <li><img src="<?php echo get_stylesheet_directory_uri() ?>/icons/phoneicon.svg" alt=""><p class="d-inline"> 074 123456789</p></li>
    <li><img src="<?php echo get_stylesheet_directory_uri() ?>/icons/mail.svg" alt=""><p class="d-inline"> Info@xocoa.nl</p></li>
  </ul>
</div>