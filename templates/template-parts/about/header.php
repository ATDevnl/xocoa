<div class="about__image--container">
  <img src="<?php the_field('about_background_icon') ?>" alt=""class="img-fluid about__image--image">
</div>

<div class="container about">
  <div class="header d-flex align-items-center">
    <div class="text-md-center">
      <?php the_field('page_title'); ?>
      <?php the_field('about_title') ?>
      <p class="text-container"><?php the_field('about_intro_text') ?></p>
    </div>
    
  </div>
</div>
