<div class="container about__blocks">
  <div class="row">
    <div class="col-md-5 text-md-center">
      <h2><?php the_field('first_heading') ?></h2>
      <p><?php the_field('first_text') ?></p>
    </div>
    <div class="col-md-5 text-md-center offset-md-2 lastblock">
      <h2><?php the_field('second_heading') ?></h2>
      <p><?php the_field('second_text') ?></p>
    </div>
  </div>
</div>