
<div class="header container d-flex align-items-center">
  <div class="row align-items-center header__relative">
    <div class="col-md-5">
    <h1><?php the_title(); ?></h1>
    <?php 
      if ( have_posts() ) {
        wp_reset_query();
        setup_postdata($post); 
        echo esc_attr(htmlentities(the_content()));
      } ;
    ?>
    </div>
    <div class="col-md-7 header__featured">
      <?php 
      echo the_post_thumbnail ('full', array (
        'class' => 'img-fluid float-right',
        'data-id' => 'dataidhere'
      ));
    ?>
    </div>
  </div>
  
</div>