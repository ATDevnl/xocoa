<div class="flavours container">
<div class="row flex-column-reverse flex-md-row">
  
    <div class="col-md-5">
      <h2><?php the_field('flavour-title')?></h2>
      <p><?php the_field('flavour-text')?></p>
      <div class="flavours__btns">
        <button class="btn btn-small btn-primary switcher"><?php the_field('first_product_title')?></button><button class="btn btn-small btn-secundary switcher toggler"><?php the_field('second_product_title')?></button>
      </div>
      <div class="flavours__go">
        <a href="<?php the_field('first_product_link')?>" class="icon-btn first">Let me get one <img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Arrow.svg" alt="">
        <a href="<?php the_field('second_product_link')?>" class="icon-btn second d-none">Let me get one <img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Arrow.svg" alt="">
        </a>
      </div>
    </div>
    <div class="col-md-6 offset-md-1 flavours__chocolate first">
      <img src="<?php the_field('First_product_image')?>" alt="" class="img-fluid">
      <h3><?php the_field('first_product_title')?></h3>
    </div>

    <div class="col-md-6 offset-md-1 flavours__chocolate second d-none">
      <img src="<?php the_field('second_product_image')?>" alt="" class="img-fluid">
      <h3><?php the_field('second_product_title')?></h3>
    </div>
    
  </div>
</div>