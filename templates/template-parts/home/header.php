<div class="header d-flex align-items-center">
  <div class="header--container">
    <?php the_field('page_title'); ?>
    <a href="<?php the_field('button_link') ?>" class="btn btn-primary"><?php the_field('button_text') ?></a>
  </div>
</div>