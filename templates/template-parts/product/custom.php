<div class="container">
  <div class="row product__attr">
    <div class="col-md-12 product__attr--top">
      <div class="product__attr__desc">
        <h3>About this bar</h3>
        <p><?php the_field('product_description'); ?></p>
      </div>
      <div class="product__attr__ean">
        <p>EAN code: <?php the_field('ean_code'); ?></p>
      </div>
    </div>
    <div class="col-md-5 product__attr__in d-md-block d-none">
      <h4>Whats in it</h4>
      <p><?php the_field('what_is_in_it'); ?></p>
    </div>
    <div class="col-md-7 product__attr__nutrion d-md-block d-none">
      <h4>Nutritional values</h4>
      <?php

      // check if the repeater field has rows of data
      if( have_rows('nutritional_valules') ):

        // loop through the rows of data
          while ( have_rows('nutritional_valules') ) : the_row();

              // display a sub field value
              echo "<p>";
              the_sub_field('nutrition');
              echo " - ";
              the_sub_field('value');
              echo "</p>";

          endwhile;

      else :

          // no rows found

      endif;

      ?>
    </div>
  </div>
</div>