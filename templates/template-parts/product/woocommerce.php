<div class="container header product--header">
  <div class="xo-heading d-md-block d-none" id="prependTitle">
    <h1 class="woocommerce-product-title" id="woocommerce-product-title"><?php the_title(); ?></h1>
    <p class="product-price">From: <b><?php the_field('from_price')?></b></p>
  </div>

  <div class="xo-heading d-md-none d-block" id="prependTitle">
    <h1 class="woocommerce-product-title" id="woocommerce-product-title"><?php the_title(); ?></h1>
    <p class="product-price">From: <b><?php the_field('from_price')?></b></p>
  </div>
  <?php
    the_content(); 
  ?>
</div>