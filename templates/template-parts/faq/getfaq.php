<div class="container faq">
  <div class="row">
    <!-- index -->
    <div class="col-md-3 faq__index">
      <h4 class="faq__index--title">Index</h4>
    <?php
      $args = array('parent' => 28);
      $categories = get_categories( $args );
      foreach($categories as $category) : ?>
       <a href="#cat<?php echo $category->term_id ?>" ><?php echo $category->name ?> </a> 
      <?php endforeach?>
    </div>
    <!-- post section -->
    <div class="col-md-9">
    <?php
        $post_type = 'faq';
        $taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );

        foreach( $taxonomies as $taxonomy ) :
        
            $terms = get_terms( $taxonomy );

            foreach( $terms as $term ) : ?>
            <?php if($term->parent == 28) : ?>
                <?php
                $args = array(
                    'post_type' => $post_type,
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => $taxonomy,
                            'field' => 'slug',
                            'terms' => $term->slug,
                        )
                    )

                );
                $posts = new WP_Query($args);

                if( $posts->have_posts() ): ?>
                
                        <section class="faq__block" id="cat<?php echo $term->term_id; ?>">
                            <h2 class="faq__block--title"><?php echo $term->name; ?> </h2>
                                <div class="row">
                                    <?php while( $posts->have_posts() ) : $posts->the_post(); ?>
                                    <a class="col-md-4 col-6 text-center faq__item--container"  href="<?php the_permalink()?>" >
                                      <div class="faq__item d-flex align-items-center">
                                        <div class="faq__item--title">
                                            <h3><?php the_title(); ?></h3>
                                        </div>
                                      </div>
                                   </a>
                                    <?php endwhile; endif; ?>
                        </section>

                <?php endif; endforeach;

        endforeach; ?>
    </div>
  </div>
</div>