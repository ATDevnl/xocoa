<div class="container search">
  <h2>Find your question</h2>
  <div id="search-form" class="search__container">
      <form class="search__form" action="/" method="get" role="search">
          <label class="search__label">
              <input class="search__label--field" type="search" name="s" value="" placeholder="Search Faq">
          </label>
          <input type="hidden" name="post_type" value="faq" />
          <button class="btn btn-icon search__submit" type="submit" ><img src="<?php echo get_stylesheet_directory_uri() ?>/icons/Arrow.svg" alt=""></button>
      </form>
</div>
</div>