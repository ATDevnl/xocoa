<?php
/**
* Template Name: Honest storys page
*/ 

get_header(); ?>


<?php get_template_part('templates/template-parts/global/header') ?>

<div class="container-fluid bg--secundary ">
  <?php get_template_part('templates/template-parts/honest/featured') ?>
</div>

<?php get_template_part('templates/template-parts/honest/allstories') ?>


<?php get_footer(); ?>