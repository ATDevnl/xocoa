<?php
/**
* Template Name: Faq page
*/ 

get_header(); ?>


<?php get_template_part('templates/template-parts/global/header') ?>

<div class="container-fluid bg--secundary ">
  <?php get_template_part('templates/template-parts/faq/search') ?>
</div>

<?php get_template_part('templates/template-parts/faq/getfaq') ?>

<?php get_footer(); ?>