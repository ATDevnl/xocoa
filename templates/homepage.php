<?php
/**
* Template Name: Home page
*/ 

get_header(); ?>

<div class="container">
  <?php get_template_part('templates/template-parts/home/header') ?>
</div>

<div class="container-fluid bg--secundary ">
  <?php get_template_part('templates/template-parts/home/flavour') ?>
</div>

<div class="container storieshome">
  <?php get_template_part('templates/template-parts/home/last-stories') ?>
</div>

<?php get_footer(); ?>