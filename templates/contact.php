<?php
/**
* Template Name: Contact page
*/ 

get_header(); ?>


<?php get_template_part('templates/template-parts/global/header') ?>

<div class="container-fluid bg--secundary contact__container">
  <?php get_template_part( 'templates/template-parts/contact/info' ) ?>
  <?php get_template_part( 'templates/template-parts/contact/form' ) ?>
</div>

<?php get_footer(); ?> 