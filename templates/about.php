<?php
/**
* Template Name: About page
*/ 

get_header(); ?>


<?php get_template_part('templates/template-parts/about/header') ?>

<div class="container-fluid bg--secundary ">
  <?php get_template_part('templates/template-parts/about/two-block-sections') ?>
</div>

<?php get_footer(); ?>