<?php
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/enqueue.php' );
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/supports.php' );
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/menus.php' );
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/custom-posts.php' );
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/search-filters.php' );

add_action( 'back_button', 'wpse221640_back_button' );
function wpse221640_back_button()
{
    if ( wp_get_referer() )
    {
        $back_text = __( '&laquo; Back' );
        $button    = "\n<button id='my-back-button' class='btn button my-back-button' onclick='javascript:history.back()'>$back_text</button>";
        echo ( $button );
    }
}