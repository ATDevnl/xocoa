<?php 
get_header(); 
the_post(); 
?>

<?php get_template_part('templates/template-parts/product/woocommerce');?>

<div class="container-fluid bg--secundary product__attr--container">
  <?php get_template_part('templates/template-parts/product/custom');?>
</div>

<div class="container d-md-none d-block">
  <div class="product__attr__in">
        <h4>Whats in it</h4>
        <p><?php the_field('what_is_in_it'); ?></p>
      </div>
      <div class="product__attr__nutrion">
        <h4>Nutritional values</h4>
        <?php

        // check if the repeater field has rows of data
        if( have_rows('nutritional_valules') ):

          // loop through the rows of data
            while ( have_rows('nutritional_valules') ) : the_row();

                // display a sub field value
                echo "<p>";
                the_sub_field('nutrition');
                echo " - ";
                the_sub_field('value');
                echo "</p>";

            endwhile;

        else :

            // no rows found

        endif;

        ?>
    </div>
</div>


<?php get_footer(); ?>